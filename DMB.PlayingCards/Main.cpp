// Playing Cards
// David Berken

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
enum Suit {Hearts, Diamonds, Spades, Clubs};
struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(const Card &card)
{
	cout << "The " << card.Rank << " of " << card.Suit << endl;
}

//Card HighCard(Card card1, Card card2) 
//{
//	//i don't know i don'tknowidontknowidontknowitdontknowidontknowidontknowidontknowidont
//}

string Suit_String(Suit card)
{
	switch (card)
	{
	case Hearts:
		return "Hearts";
		break;
	case Diamonds:
		return "Diamonds";
		break;
	case Spades:
		return "Spades";
		break;
	case Clubs:
		return "Hearts";
		break;
	default:
		return "No card";
		break;
	}
}

int main()
{
	Card card1;
	card1.Rank = Five;
	card1.Suit = Diamonds;
	PrintCard(card1);

	/*cout << "The " << card1.Rank << " of " << card1.Suit;*/

	(void)_getch();
	return 0;
}